﻿using UnityEngine;
using System.Collections;

public class MakeMaterialUnique : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var mat = GetComponent<MeshRenderer>().material;
		mat.name = GetComponent<MeshRenderer>().sharedMaterial.name + "-" + Random.Range(1000, 9999);
		GetComponent<MeshRenderer>().material = mat;
	}
}

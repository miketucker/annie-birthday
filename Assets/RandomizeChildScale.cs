﻿using UnityEngine;
using System.Collections;

public class RandomizeChildScale : MonoBehaviour {
	public Transform target;
	public RangeFloat xzScale = new RangeFloat(0.5f, 1.5f);
	// Use this for initialization
	void OnEnable() {
		float n = xzScale.random;
		target.localScale = new Vector3(n, target.localScale.y, n);
		enabled = false;
	}
	
}

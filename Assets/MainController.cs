﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainController : MonoBehaviour {
	
	public static MainController instance;

	public AudioSource audioTarget;
	public Kvant.WigController wigController;
	public Material wigMaterial;
	public Light mainLight;
	public bool playTimeline = false;
	public float skipToTime = 0f;
	public AudioSource[] sources;
	public SineMove[] movements;
	public Rotate spinner;
	public Kino.Bloom bloom;

	public LerpFollow lerpFollow;
	int currentEventIndex = -1;
	int nextEventIndex = 0;
	bool needsRestart = false;

	public TimeEvent[] events = new TimeEvent[]
	{
		new TimeEvent() {time = 0f, name = "intro" },
		new TimeEvent() {time = 42f, name = "endIntro" },
		new TimeEvent() {time = 45f, name = "middle" },
		new TimeEvent() {time = 52f, name = "bridge" },
		new TimeEvent() {time = 76f, name = "max" },
		new TimeEvent() {time = 88f, name = "slowing" },
		new TimeEvent() {time = 105f, name = "bass" },
		new TimeEvent() {time = 116f, name = "end" },
	};

	TimeEvent currentEvent = null;

	[System.Serializable]
	public class TimeEvent {
		public string name = "";
		public float time = 0f;
	}


	public ProcessorSettings[] settings;

	[System.Serializable]
	public class ProcessorSettings {
		public AudioProcessor pr;
		public List<Material> mats;
		public float threshold = 0.5f;
		public Gradient gradient;
		public RangeFloat durationRange = new RangeFloat(0f, 0.1f);
		public bool isEnabled = true;
		public bool doDebug = false;
		public Color clearColor = Color.black;
		public Material randomMat {
			get { return mats[Random.Range(0, mats.Count)]; }
		}
	}

	public WigSettings[] wigSettings;

	[System.Serializable]
	public class WigSettings {
		public string name = "untitled";
		public float length;
		public float lightIntensity = 0f;
		public float glowProb = 0f;
		public float spring = 600f;
		public float damping = 30f;

		public float noiseAmp = 6f;
		public float noiseFreq = 1f;
		public float noiseSpeed = 0.1f;

		public WigSettings(Kvant.WigController wc, Material wigMat, Light light) {
			CopyFrom(wc, wigMat, light);
		}

		public void Apply(Kvant.WigController wc , Material wigMat, Light light) {
			wc.length = length;
			wc.spring = spring;
			wc.damping = damping;

			light.intensity = lightIntensity;
			wigMat.SetFloat("_GlowProb", glowProb);
		}

		public void CopyFrom(Kvant.WigController wc, Material wigMat, Light light) {
			length = wc.length;
			spring = wc.spring;
			damping = wc.damping;
			glowProb = wigMat.GetFloat("_GlowProb");
			lightIntensity = light.intensity;
		}

		public static void Lerp(WigSettings a, WigSettings b, float t, Kvant.WigController wc, Material wigMat, Light light) {
			wc.length = Mathf.Lerp(a.length, b.length, t);
			wc.spring = Mathf.Lerp(a.spring, b.spring, t);
			wc.damping = Mathf.Lerp(a.damping, b.damping, t);
			wigMat.SetFloat("_GlowProb", Mathf.Lerp(a.glowProb, b.glowProb, t));
			light.intensity = Mathf.Lerp(a.lightIntensity, b.lightIntensity, t);
		}
	}

	string propKey = "_Color";

	void Awake() {
		instance = this;
	}

	void Start() {
		if (skipToTime > 0f) {
			foreach(AudioSource s in sources) {
				s.time = skipToTime;
			}

		}
	}
	
	void Update() {
		if (playTimeline) CheckEvent();

		foreach (ProcessorSettings ps in settings) {
			if (ps.isEnabled) {
				if (ps.pr.GetRMS() > ps.threshold) {
					StartCoroutine(Flash(ps));
				}
			}
		}
	}

	IEnumerator Flash(ProcessorSettings ps) {
		Material mat = ps.randomMat;
		mat.SetColor(propKey, ps.gradient.Evaluate(Random.value));
		yield return new WaitForSeconds(ps.durationRange.random);
		mat.SetColor(propKey, ps.clearColor);
	}

	IEnumerator LerpWig(WigSettings ws, float duration = 3f) {
		float startTime = Time.time;
		

		Debug.Log("transition to wig: " + ws.name);
		WigSettings currentWs = new WigSettings(wigController, wigMaterial,mainLight);
		while(Time.time - startTime < duration) {
			float t = (Time.time - startTime) / duration;

			WigSettings.Lerp(currentWs, ws, t, wigController, wigMaterial, mainLight);

			yield return null;

		}
	}

	void SetEvent(TimeEvent e) {
		currentEvent = e;
		currentEventIndex = System.Array.IndexOf(events,e);
		if (currentEventIndex + 1 >= events.Length) {
			nextEventIndex = 0;
			needsRestart = true;
		}
		else nextEventIndex = currentEventIndex + 1;

		Debug.Log("event: " + currentEvent.name);

		switch (currentEvent.name) {
			case "intro":
				wigController.gameObject.SetActive(false);
				settings[0].isEnabled = true;
				settings[1].isEnabled = false;
				settings[2].isEnabled = false;
				lerpFollow.SwitchTo(0f);
				UpdateMovementSpeed(0.1f);
				LerpBloom(1f);
				//WigSettingByName("hidden").Apply(wigController, wigMaterial, mainLight);
				break;
			case "intro2":
				settings[1].isEnabled = true;
				LerpBloom(20f,10f);
				break;
			case "endIntro":
				settings[0].isEnabled = false;
				settings[1].isEnabled = false;
				wigController.gameObject.SetActive(true);
				StartCoroutine(LerpWig(WigSettingByName("short")));
				LerpBloom(1f, 3f);

				LerpMovementSpeed(0.5f);

				break;
			case "middle":
				StartCoroutine(LerpWig(WigSettingByName("middle")));
				LerpSpinner(10f);

				break;

			case "bridge":
				StartCoroutine(LerpWig(WigSettingByName("strobe")));
				lerpFollow.SwitchTo(1f);

				//LerpSpinner(10f);
				break;

			case "max":
				StartCoroutine(LerpWig(WigSettingByName("max")));
				settings[2].isEnabled = true;
				LerpSpinner(200f);
				break;

			case "slowing":

				StartCoroutine(LerpWig(WigSettingByName("slowing")));
				LerpSpinner(1f);
				settings[2].isEnabled = true;
				LerpBloom(10f, 5f);
				break;


			case "bass":
				settings[2].isEnabled = true;
				LerpSpinner(1f);
				LerpBloom(0.5f, 3f);
				StartCoroutine(LerpWig(WigSettingByName("hidden"),8f));
				break;
			case "end":
			
				//settings[2].isEnabled = true;
				break;


		}




	}



	public void RegisterMat(string kind, Material mat) {


		switch (kind) {
			case "capsule":
				settings[0].mats.Add(mat);
				mat.SetColor(propKey, settings[0].clearColor);
				break;
			case "line":
				settings[1].mats.Add(mat);
				mat.SetColor(propKey, settings[1].clearColor);
				break;
			case "text":
				settings[2].mats.Add(mat);
				mat.SetColor(propKey, settings[2].clearColor);
				break;
		}
	}

	float lastFrameAudioTime = -1f;

	void CheckEvent() {
		if (needsRestart) {
			if (audioTarget.time < lastFrameAudioTime) needsRestart = false;
		}

		if (currentEvent == null) SetEvent(events[0]);
		else if (audioTarget.time >= events[nextEventIndex].time && !needsRestart) SetEvent(events[nextEventIndex]);

		lastFrameAudioTime = audioTarget.time;

	}


	void LerpBloom(float target, float duration = 3f) {
		float start = bloom.intensity;
		iTween.ValueTo(gameObject, iTween.Hash("from", start, "to", target, "time", duration, "onupdate", "UpdateBloom"));

	}

	void UpdateBloom(float n) {
		bloom.intensity = n;
		//Debug.Log("update spinner " + n);
	}

	void LerpSpinner(float target) {
		float start = spinner.multiplier;
		iTween.ValueTo(gameObject, iTween.Hash("from", start, "to", target, "time", 3f, "onupdate", "UpdateSpinner"));

	}

	void UpdateSpinner(float n) {
		spinner.multiplier = n;
		//Debug.Log("update spinner " + n);
	}

	void LerpMovementSpeed(float target) {
		float start = movements[0].globalSpeed;
		iTween.ValueTo(gameObject, iTween.Hash("from", start, "to", target, "time", 3f, "onupdate", "UpdateMovementSpeed"));

	}

	void UpdateMovementSpeed(float n) {
		foreach(SineMove m in movements) {
			m.globalSpeed = n;
		}
	}

	WigSettings WigSettingByName(string n) {
		foreach (WigSettings s in wigSettings) {
			if (s.name == n) return s;
		}
		return null;

	}


	TimeEvent EventByName(string n) {
		foreach (var e in events) {
			if (e.name == n) return e;
		}
		return null;

	}

}
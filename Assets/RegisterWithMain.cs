﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshRenderer))]
public class RegisterWithMain : MonoBehaviour {
	public string kind = "capsule";

	// Use this for initialization
	void Start () {
		MainController.instance.RegisterMat(kind, GetComponent<MeshRenderer>().sharedMaterial);
		enabled = false;
	}
	
}

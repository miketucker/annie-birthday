﻿using UnityEngine;
using System.Collections;

public class FadeLightToRMS : MonoBehaviour {

	public Color colorA;
	public Color colorB;
	public AudioProcessor audioProcessor;
	public RangeFloat range = new RangeFloat(0f, 0.2f);
	public string prop = "_Color";
	Material mat;

	// Use this for initialization
	void Start () {
		mat = GetComponent<MeshRenderer>().sharedMaterial;
	}
	
	// Update is called once per frame
	void Update () {
		mat.SetColor(prop,Color.Lerp(colorA, colorB, range.PointInRange(audioProcessor.GetRMS())));
	}
}

﻿using UnityEngine;
using System.Collections;

public class RandomizeDrag : MonoBehaviour {
	public RangeFloat dragRange = new RangeFloat(0.03f, 0.2f);
	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().drag = dragRange.random;
	}
	
}

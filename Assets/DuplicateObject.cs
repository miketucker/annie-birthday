﻿using UnityEngine;
using System.Collections;

public class DuplicateObject : MonoBehaviour {
	public GameObject target;
	public int count = 10;
	public float rotationSpeed = 50f;
	// Use this for initialization
	void Start() {
		for (int i = 0; i < count; i++) {
			var obj = Instantiate(target) as GameObject;
			obj.transform.parent = target.transform.parent;
			obj.transform.localPosition = Vector3.zero;
			obj.transform.rotation = Random.rotation;
			obj.GetComponent<Rotate>().amount = Random.onUnitSphere * 50f;
			if (!obj.activeSelf) obj.SetActive(true);
		}
		enabled = false;
	}
}

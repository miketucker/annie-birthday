﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WigModifier : MonoBehaviour {

	public Kvant.WigController template;

	public List<Kvant.WigController> copies;
	public bool doUpdate;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (doUpdate) {

			foreach(Kvant.WigController w in copies) {
				w.length = template.length;
				w.lengthRandomness = template.lengthRandomness;
				w.spring = template.spring;
				w.damping = template.damping;
				w.gravity = template.gravity;


				w.noiseAmplitude = template.noiseAmplitude;
				w.noiseFrequency = template.noiseFrequency;
				w.noiseSpeed = template.noiseSpeed;

				
			}

			//doUpdate = false;
		}
	}
}

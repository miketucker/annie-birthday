﻿using UnityEngine;
using System.Collections;

public class LerpFollow : MonoBehaviour {
	public Transform targetA;
	public Transform targetB;

	public float t = 0f;
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp(targetA.position, targetB.position, t);
	}

	public void SwitchTo(float n, float duration = 3f) {
		iTween.ValueTo(gameObject, iTween.Hash("from",t, "to", n, "time", duration, "easetype", "easeInOutQuad", "onupdate", "UpdateSwitch"));
	}

	void UpdateSwitch(float n) {
		t = n;
	}
}

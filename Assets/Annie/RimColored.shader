﻿Shader "Custom/Rim Colored" 
{
	Properties 
	{
		_Color ("Base Color", Color) = (.5,.5,.5,1)
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower ("Rim Power", Range(0.1,8.0)) = 3.0
		_RimStrength("Rim Strength", Range(0.0,4.0)) = 0.25
		_AmbientLight ("Ambient Light", Range(0.0,1.0)) = 0.25
		_AmbientColor("Ambient Color", Color) = (0.0,0.0,0.0,1.0)
	}
	SubShader 
	{
		Tags { "Queue" = "Transparent" }

		//
		// This pass only writes to the depth buffer, incorporating the vertex modification
		//
		Pass {
			ZWrite On
			ColorMask 0

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			float _ZFreq;
			float _NormalAmp;
			float _Offset;

			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v) {
				v2f o;
				//v.vertex.xyz += v.normal * (sin(_Time.y + _Offset + v.vertex.z * _ZFreq) * _NormalAmp + (_NormalAmp * 0.5));
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target {
				return fixed4 (1.0, 1.0, 1.0, 1.0);
			}

				ENDCG
	}

		CGPROGRAM
#pragma surface surf Lambert vertex:vert alpha:fade
#pragma exclude_renderers d3d11_9x
				float4 _Color;
		float4 _RimColor;
		float4 _AmbientColor;
		float _RimPower;
		float _AmbientLight;
		float _RimStrength;
		struct Input
		{
			/*
			float4 albedoColor;
			float3 rimColor;*/
			float3 viewDir;

		};

		struct v2f
		{
			float2 uv : TEXCOORD0;
		};

		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);

			//o.albedoColor = _Color;// lerp(_InnerColor, _OuterColor, abs(sin(length(v.vertex) + abs(sin(_Time.y)))));
			//o.rimColor = lerp(_RimColor,_RimOuterColor,abs(sin(length(v.vertex) + sin(_Time.y) ) ));
			//o.rimColor = _RimColor;
			//v.customColor = float2(_GradOffset,_GradOffset);

			//v.vertex.xyz += v.normal * (sin(_Time.y + _Offset + v.vertex.z * _ZFreq) * _NormalAmp + ( _NormalAmp * 0.5));
		}


		void surf (Input IN, inout SurfaceOutput o)
		{
			half rim = 1.0 - saturate(dot(normalize(IN.viewDir), o.Normal));
			half p = pow(rim, _RimPower);
			o.Albedo = lerp(_AmbientColor,_Color,p);
			o.Alpha = _Color.a + p;
			o.Emission = lerp(_AmbientColor,_Color * _RimColor * (_AmbientLight + ((_RimStrength))),p);

			//o.Emission = float4(IN.customColor.x,IN.customColor.x,1,1);//o.Albedo;
			//o.Emission = o.Albedo;
		}
		ENDCG
	}
	Fallback "Diffuse"
}
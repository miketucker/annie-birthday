﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Unitilities.Tuples;

public class GravityBall {
	public Transform transform;
	public Rigidbody rb;
	public float pull;
	public Color color;
	public GameObject gameObject;
	public int connections;
	public int index;
	public float audioStartTime = 0f;
	internal Color originalColor;
}

public class GravityBalls : MonoBehaviour {
	public int count = 50;
	public float spawnArea = 0.2f;
	public float magnetRange = 0.1f;
	public GameObject prefab;
	public Transform pullTarget;
	public RangeFloat pullRange = new RangeFloat( 0.2f, 1f );
	public RangeFloat scaleRange = new RangeFloat( 0.75f, 1.25f );
	public float pullMultiplier = 1f;
	public List<Transform> targets = new List<Transform>();
	public float forwardOffset = 0.05f;
	public bool doNeighborLines;
	public RangeFloat neighborDist = new RangeFloat(0.03f,0.2f);
	public RangeFloat neighborDistChance = new RangeFloat( 0.1f, 1f );
	public Color[] neighborColorPalette;
	public Color[] highlightColorPalette;
	public float neighborColorAlpha = 0.2f;
	GravityBall[] balls;
	HashSet<Tuple<GravityBall, GravityBall>> activeBallPairs = new HashSet<Tuple<GravityBall, GravityBall>>();
	public float centerPullStrength = 0.05f;
	public float probabilityOffset = -0.5f;

	// flow
	public Flow3D flow;
	public Flow3D connectionProbabilityFlow;

	public GameObject soundObject;
	public GameObject motionSoundObject;
	[Range (0f, 1f)]
	public float _twinkleVolume = .25f;

	// Use this for initialization
	void Start () {
		if (!pullTarget) pullTarget = transform;
		

		balls = new GravityBall[count];
		for ( int i = 0; i < count; i++ ) {
			GameObject g = Instantiate( prefab ) as GameObject;
			g.transform.parent = transform;
			g.transform.localScale *= scaleRange.random;
			g.transform.localPosition = Random.insideUnitSphere * spawnArea;
			g.SetActive( true );
			Color c = neighborColorPalette[Random.Range( 0, neighborColorPalette.Length )];

			Transform targetObj = g.transform;
			var wc = g.GetComponent<WigCombo>();
			if (wc) {
				targetObj = wc.target;
			}

			balls[i] = new GravityBall() {
				rb = targetObj.GetComponent<Rigidbody>(),
				transform = targetObj,
				pull = pullRange.random,
				originalColor = c,
				color = c,
				gameObject = g,
				connections = 0,
				index = i
			};
					

			balls[i].color.a = neighborColorAlpha;


			/*
			// Remove audiosources from most balls
			if (i%5>0) {
				Destroy(g.GetComponent<TBE.Components.TBE_SourceControl>());
				Destroy(g.GetComponent<AudioSource>());
				Destroy(g.GetComponent<AudioReverbFilter>());
			}
			*/
		}
	}

	void OnEnable() {
		if(doNeighborLines) StartCoroutine( NeighborLines() );
	}
	

	float[] pitchModifiers = {1f, 1.333f, 1.5f, 2f};
	// float[] pitchModifiers = {1f, 1.25f, 1.333f, 1.5f, 2f};
	// float[] pitchModifiers = {1f, 1.066f, 1.125f, 1.2f, 1.25f, 1.333f, 1.4f, 1.5f, 1.6f, 1.667f, 1.777f, 1.875f, 2f};
	// float[] pitchModifiers = {1f, 1.125f, 1.25f, 1.333f, 1.5f, 1.667f, 1.875f, 2f};
	
	int pitchInd = 0;

	IEnumerator NeighborLines() {
		yield return null;

		// Called when scene is loaded

		// prep every ball combination in the scene

		List<Tuple<GravityBall, GravityBall>> allPairsOfBalls = new List<Tuple<GravityBall, GravityBall>>( );
		for ( int i = 0; i < balls.Length; i++ ) {
			for ( int k = i+1; k < balls.Length; k++ ) {
				allPairsOfBalls.Add( new Tuple<GravityBall, GravityBall>( balls[i], balls[k] ) );
			}
		}
		int totalBallCount = allPairsOfBalls.Count;
		int checksPerFrame = totalBallCount / 10;
		int j = 0;

		// prepare mesh

		Mesh mesh = new Mesh();
		mesh.MarkDynamic();
		List<Vector3> verts = new List<Vector3>();
		List<int> inds = new List<int>();
		List<Color> cols = new List<Color>();
		Bounds meshBounds = new Bounds( Vector3.zero, new Vector3( 100000f, 100000f, 100000f ) );
		GetComponent<MeshFilter>().sharedMesh = mesh;


		// loop recurrs every frame

		while ( enabled ) {
			float flucPercent = Mathf.Abs( Mathf.Cos( Time.time * 0.1f ) );
			float distCheck = neighborDist.Percent( flucPercent );
			float distChance = neighborDistChance.Percent( flucPercent ) + probabilityOffset;
			// Find neighboring balls


			int[] oldConnections = new int[balls.Length];
			int[] newConnections = new int[balls.Length];
			for (int i = 0; i < balls.Length; i++) {
				oldConnections[i] = balls[i].connections;
				newConnections[i] = balls[i].connections;
			}


			for ( int i = 0; i < checksPerFrame; i++ ) {
				var t = allPairsOfBalls[j];

				float probability = ( connectionProbabilityFlow ) ? connectionProbabilityFlow.SampleFloat( t.first.rb.position ): Random.value;

				if ( ( distChance > probability) && ( t.first.rb.position - t.second.rb.position ).sqrMagnitude < distCheck ) {
				//if ( ( t.first.rb.position - t.second.rb.position ).sqrMagnitude < distCheck ) {


					if (!activeBallPairs.Contains(t)) {
						activeBallPairs.Add( t );
						t.first.connections++;
						t.second.connections++;

						newConnections[t.first.index]++;
						newConnections[t.second.index]++;
					}

				} else {

					if (activeBallPairs.Contains(t)) {
						activeBallPairs.Remove( t );
						t.first.connections--;
						t.second.connections--;

						newConnections[t.first.index]--;
						newConnections[t.second.index]--;
					}
				}
				j = ( j + 1 ) % totalBallCount;
			}

			int threshold = 8;
			for (int i = 0; i < newConnections.Length; i++) {

				int diff = newConnections[i] - oldConnections[i];

				AudioSource audioSource = balls[i].gameObject.GetComponent<AudioSource>();

				if (!audioSource) continue;

				if (diff > threshold) {
					if (!audioSource.isPlaying) {
						balls[i].audioStartTime = Time.time;
						audioSource.time = Random.Range(0f, audioSource.clip.length);

						balls[i].color = highlightColorPalette[Random.Range( 0, highlightColorPalette.Length )];

						// pitchInd = (pitchInd + 1) % pitchModifiers.Length;
						pitchInd = Random.Range(0, pitchModifiers.Length);
						//float fraction = Time.time - Mathf.Floor(Time.time);
						//int index = (int)Mathf.Floor(fraction);
						//int pitchInd = index % pitchModifiers.Length;
						// pitchInd = 0;

						audioSource.pitch = pitchModifiers[pitchInd];
						audioSource.Play();
					}
				} else if (diff < - 4 * threshold) {
					if (!audioSource.isPlaying) {
						balls[i].audioStartTime = Time.time;
						audioSource.time = Random.Range(0f, audioSource.clip.length);
						audioSource.pitch = .5f;
						audioSource.Play();
					}
				} else {

					audioSource.volume = _twinkleVolume - _twinkleVolume * (Time.time - balls[i].audioStartTime) / .25f;
						if ( audioSource.volume <= 0f ) {
							balls[i].color = balls[i].originalColor;
							audioSource.Pause();
						}

				}
			}

			// Regenerate mesh

			verts.Clear();
			inds.Clear();
			mesh.Clear();
			cols.Clear();
			mesh.bounds = meshBounds;
			int indexCount = 0;

			foreach ( var t in activeBallPairs ) {
				verts.Add( t.first.transform.localPosition );
				verts.Add( t.second.transform.localPosition );
				inds.Add( indexCount );
				cols.Add( t.first.color );
				indexCount++;
				inds.Add( indexCount );
				cols.Add( t.second.color );
				indexCount++;
			}

			mesh.vertices = verts.ToArray();
			mesh.colors = cols.ToArray();
			mesh.SetIndices( inds.ToArray(), MeshTopology.Lines, 0 );

			// end of frame
			yield return null;
		}
	}

	//void OnHandRegister( HandController hc ) {
	//	Debug.Log( "GravityBall: register hand " + hc.HandIndex );
	//	if ( !targets.Contains( hc.transform ) )
	//		targets.Add( hc.transform );
	//}
	//void OnHandUnregister( HandController hc ) {
	//	Debug.Log( "GravityBall: unregister hand " + hc.HandIndex );
	//	if ( targets.Contains( hc.transform ) )
	//		targets.Remove( hc.transform );
	//}


	void UpdateSoundObject() {

		Vector3 averagePosition = new Vector3();
		foreach (GravityBall gb in balls) {
			averagePosition += gb.gameObject.transform.position;
		}
		averagePosition /= balls.Length;
		soundObject.transform.position = averagePosition;
		motionSoundObject.transform.position = averagePosition;

		float spread = 0f;
		foreach (GravityBall gb in balls) {
			Vector3 pos = gb.gameObject.transform.position;
			spread += Mathf.Abs(pos.x) + Mathf.Abs(pos.y) + Mathf.Abs(pos.z);
		}

		AudioSource audioSource = soundObject.GetComponent<AudioSource>();
		float spreadFactor = Mathf.Clamp01(1f - (spread - 500f) / 300f);
		audioSource.volume = spreadFactor;


		float velocityMag = 0f;
		foreach (GravityBall gb in balls) {
			Rigidbody rb = gb.gameObject.GetComponent<Rigidbody>();
			velocityMag += rb.velocity.magnitude;
		}

		audioSource = motionSoundObject.GetComponent<AudioSource>();

		float motionFactor = Mathf.Clamp01((velocityMag - 50f) / 200f);
		audioSource.volume = spreadFactor * motionFactor;


	}


	void OnGUI(){
		GUILayout.Space (200);
		GUILayout.Label ("Neighbor alpha: " + neighborColorAlpha);
	}

	void UpdateColorAlpha(){
		for (int i = 0; i < balls.Length; i++) {
			balls [i].color.a = neighborColorAlpha;
		}

	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKey (KeyCode.UpArrow)) {
			neighborColorAlpha += 0.01f;
			UpdateColorAlpha ();
		}
		if (Input.GetKey (KeyCode.UpArrow)) {
			neighborColorAlpha -= 0.01f;
			UpdateColorAlpha ();
		}



		// add controllers that are currently pressed

		foreach(HandController hc in HandController.Hands ) {
			float pullAmount = pullMultiplier * Time.deltaTime * hc.buttonState.triggerValue;
			float rangeSqr = spawnArea * spawnArea * hc.buttonState.triggerValue;
			Transform target = hc.transform;
			

			for ( int i = 0; i < balls.Length; i++ ) {
				float sqrD = ( balls[i].rb.position - target.position ).sqrMagnitude;
				if ( sqrD < rangeSqr ) {
					float percent = ( rangeSqr / sqrD );
					balls[i].rb.velocity += ( ( target.position + target.forward * forwardOffset ) - balls[i].rb.position ) * ( pullAmount * balls[i].pull );
				}
			}

		}

		// pull to center
		Vector3 pullPos = pullTarget.position;
		float suckToCenter = Time.deltaTime * centerPullStrength;
		for ( int i = 0; i < balls.Length; i++ ) {
			balls[i].rb.velocity += ( pullPos - balls[i].transform.position ) * (suckToCenter * balls[i].pull);
		}

		// add flow

		if(flow != null ) {
			for ( int i = 0; i < balls.Length; i++ ) {
				balls[i].rb.velocity += flow.SampleLite( balls[i].rb.position );
			}
		}

		//UpdateSoundObject();
	}
}

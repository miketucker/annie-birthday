﻿using UnityEngine;
using System.Collections;

public class AudioScale : MonoBehaviour {
	public AudioProcessor _audioProcessor;
	public int freqRange = 1;
	public RangeFloat range = new RangeFloat( 0.2f, 2f );
	void Update() {
		if (_audioProcessor && _audioProcessor._freqRanges != null) {
			freqRange = Mathf.Clamp(freqRange, 0, _audioProcessor._freqRanges.Length - 1);
			float scale = _audioProcessor._freqRanges[freqRange]._average;
			transform.localScale = Vector3.one * range.Percent(scale);
		}

	}
	
}

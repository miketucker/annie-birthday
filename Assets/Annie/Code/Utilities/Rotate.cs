﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour {

	public Vector3 amount = new Vector3(0f, 0f, 0f);
	public float multiplier = 1f;
	public bool useSine = true;
	public float sineSpeed = 1f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (useSine) {
			Vector3 n = new Vector3(
				amount.x * Mathf.Sin(sineSpeed * 0.89532f * Time.time),
				amount.y * Mathf.Cos(sineSpeed * 1.13165f * Time.time),
				amount.z * Mathf.Cos(sineSpeed * 1.4753f * Time.time)
				);
			transform.Rotate(n * (multiplier * Time.deltaTime));
		} else transform.Rotate(amount * (multiplier * Time.deltaTime));	
	}
}

﻿using UnityEngine;
using UnityEngine.Serialization;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
[CustomEditor(typeof(PlatformManager))]
public class PlatformManagerEditor : Editor {
	const string VR_KEY = "Multiplatform-VR";
	public override void OnInspectorGUI(){

		PlatformManager pm = target as PlatformManager;


		bool currentVr = UnityEditor.PlayerSettings.virtualRealitySupported;

		if(EditorPrefs.HasKey( VR_KEY ) ) {
			bool keyVal = EditorPrefs.GetBool( VR_KEY );
			if (keyVal != currentVr ) {
				UnityEditor.PlayerSettings.virtualRealitySupported = keyVal;
				currentVr = keyVal;
			}
		}

		//if ( pm.activePlatformOptions.mode != PlatformManager.Mode.Vive && currentVr ) {
		//	UnityEditor.PlayerSettings.virtualRealitySupported = false;
		//}

		//if ( pm.activePlatformOptions.mode == PlatformManager.Mode.Vive != currentVr ) {
		//	UnityEditor.PlayerSettings.virtualRealitySupported = true;
		//}

		GUILayout.Label ((currentVr) ? "VR is currently enabled" : "VR is currently disabled");

		if (currentVr) {
			if (GUILayout.Button ("Disable")) {
				UnityEditor.PlayerSettings.virtualRealitySupported = false;
				EditorPrefs.SetBool( VR_KEY, false );
			}
		} else {
			if (GUILayout.Button ("Enable")) {
				UnityEditor.PlayerSettings.virtualRealitySupported = true;
				EditorPrefs.SetBool( VR_KEY, true );
			}
		}



		if ( pm.autoSelectPlatform ) {
			RuntimePlatform currentPlatform = Application.platform;

			if ( currentPlatform == RuntimePlatform.OSXEditor ) {
				if ( pm.modeForEditor != PlatformManager.Mode.Desktop ) {
					pm.modeForEditor = PlatformManager.Mode.Desktop;
					pm.EnableCurrentPlatform();
				}
			}

			if(currentPlatform == RuntimePlatform.WindowsEditor ) {
				PlatformManager.Mode targetMode = PlatformManager.Mode.Desktop;
				if ( currentVr )
					targetMode = PlatformManager.Mode.Vive;

				if ( pm.modeForEditor != targetMode ) {
					pm.modeForEditor = targetMode;
					pm.SetActivePlatform (pm.modeForEditor);
					pm.EnableCurrentPlatform();
				}

			}
		} else {
			pm.SetActivePlatform (pm.modeForEditor);
			pm.EnableCurrentPlatform ();
		}


		DrawDefaultInspector();
	}
}

#endif


[System.Serializable]
public class PlatformOptions {
	public PlatformManager.Mode mode;
	[FormerlySerializedAs("platform")]
	public RuntimePlatform runtimePlatform;
	public GameObject gameObject;
	public Transform listenerPosition;
}




public class PlatformManager : MonoBehaviour {

	public enum Mode {
		MagicLeap, Vive, Desktop
	}

	public bool autoSelectPlatform = true;

	public Mode modeForEditor = Mode.Desktop;
	public Mode modeForBuild = Mode.MagicLeap;

	public GameObject audioListener;
	public PlatformOptions[] options;


	static PlatformManager _instance;
	private bool isInited;

	public PlatformOptions activePlatformOptions { private set; get; }

	void Awake() {
		_instance = this;
		Init();
		Startup();
	}

	void Init() {
		if ( isInited )
			return;

		isInited = true;

		if ( autoSelectPlatform ) {
			RuntimePlatform currentPlatform = Application.platform;

			if ( currentPlatform == RuntimePlatform.WindowsEditor )
				currentPlatform = RuntimePlatform.WindowsPlayer;

			Debug.Log( "Platform: " + currentPlatform );
			SetActivePlatform (currentPlatform);

		} else {
			SetActivePlatform (TargetMode);
		}
	}

	public void SetActivePlatform(Mode mode){
		foreach ( PlatformOptions o in options ) {
			if ( o.mode == mode ) {
				activePlatformOptions = o;
			}
		}
	}

	public void SetActivePlatform(RuntimePlatform runtimePlatform){
		foreach ( PlatformOptions o in options ) {
			if ( o.runtimePlatform == runtimePlatform ) {
				activePlatformOptions = o;
			}
		}

	}

	void OnValidate() {


#if UNITY_EDITOR

#endif
	}

	public void Startup() {

		EnableCurrentPlatform();

		
	}

	Mode TargetMode {
		get { 
			Mode targetMode = modeForBuild;
			#if UNITY_EDITOR
			targetMode = modeForEditor;
			#endif
			return targetMode;
		}
	}

	public void EnableCurrentPlatform() {

		// Disable any already active platforms
		foreach ( PlatformOptions o in options ) {
			if (o.gameObject != null) {
				if (o.mode != TargetMode && o.gameObject.activeSelf) {
					o.gameObject.SetActive(false);
				}
			}
		}

		// enable the one we're using
		foreach ( PlatformOptions o in options ) {
			if ( o.mode == TargetMode ) {
				if(o.gameObject != null) o.gameObject.SetActive( true );
			}
		}

		// Position listener.
		if ( audioListener != null && activePlatformOptions.listenerPosition != null ) {
			audioListener.transform.parent = activePlatformOptions.listenerPosition;
			audioListener.transform.localPosition = Vector3.zero;
			audioListener.transform.localRotation = Quaternion.identity;
		} else {
			Debug.Log( "Could not add audio listener" );
		}
	}



	public static PlatformOptions ActivePlatformOptions
	{
		get { return Instance.activePlatformOptions; }
	}

	public static PlatformManager Instance
	{
		get
		{
			if ( _instance == null ) {

				_instance = FindObjectOfType<PlatformManager>();
				_instance.Init();

			}
			return _instance;
		}
	}
}

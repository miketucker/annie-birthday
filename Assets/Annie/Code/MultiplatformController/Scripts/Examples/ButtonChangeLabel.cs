﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

[RequireComponent(typeof(InteractiveButtonObject))]
public class ButtonChangeLabel : MonoBehaviour {
	public Text textLabel;
	public string rolloverText;
	public string selectedText;

	// Use this for initialization
	void Start () {
		var button = GetComponent<InteractiveButtonObject>();
		button.OnRollover += OnRollover;
		button.OnRollout += OnRollout;
		button.OnClick += OnClick;
	}

	private void OnClick( InteractiveButtonObject obj ) {
		textLabel.text = selectedText;
	}

	private void OnRollout( InteractiveButtonObject obj ) {
		textLabel.text = "";
	}

	private void OnRollover( InteractiveButtonObject obj ) {
		textLabel.text = rolloverText;
	}
}

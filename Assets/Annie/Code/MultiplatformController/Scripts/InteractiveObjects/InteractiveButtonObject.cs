﻿using UnityEngine;
using System.Collections;


public class InteractiveButtonObject : BaseInteractiveObject {
	
	public ButtonMaterialStyle buttonStyle;
	
	Vector3 defaultScale;
	MaterialPropertyBlock rolloverMatBlock;
	MaterialPropertyBlock rolloutMatBlock;

	public System.Action<InteractiveButtonObject> OnClick;
	public System.Action<InteractiveButtonObject> OnRollover;
	public System.Action<InteractiveButtonObject> OnRollout;

	protected override void Awake() {
		base.Awake();
		defaultScale = transform.localScale;
		buttonStyle.AssignRenderer(GetComponent<Renderer>());
	}
	
	public override void OnRollOver( HandPickupInteractivity hpi ) {
		base.OnRollOver( hpi );
		if ( OnRollover != null )
			OnRollover( this );
		buttonStyle.Rollover();
	}

	public override void OnRollOut( HandPickupInteractivity hpi ) {
		base.OnRollOut( hpi );
		if ( OnRollout != null )
			OnRollout( this );
		buttonStyle.Reset();
	}

	public void AnimateIn() {
		isInteractivityEnabled = true;
		iTween.ScaleTo( gameObject, iTween.Hash( "scale", defaultScale, "time", 0.5f, "easetype", "easeOutCubic" ) );
	}

	public void AnimateOut() {
		isInteractivityEnabled = false;
		iTween.ScaleTo( gameObject, iTween.Hash( "scale", Vector3.zero, "time", 0.5f, "easetype", "easeOutCubic" ) );
	}

	public void Deactivate() {
		isInteractivityEnabled = false;
		transform.localScale = Vector3.zero;
	}

	public override bool BeginInteraction( HandPickupInteractivity hpi ) {
		// prevent this button from being picked up by not calling the base.BeginInteraction
		if (OnClick != null)
			OnClick (this);
		buttonStyle.Activate();
		return false;
	}

	public override void EndInteraction( HandPickupInteractivity hpi ) {
		return;
	}
}



[System.Serializable]
public class MaterialSetting {
	public enum Kind {
		Color, Float, Vector, Texture
	}

	public string property;							// The property you want to target in the shader (eg. "_Color" or "_MainTexture")
	public MaterialSetting.Kind kind;               // The type of property we're targeting... Color, Float, Vector, Texture
	public Color colorValue;						// the value we're changing, you only need to provide the type of property we're targeting
	public float floatValue;
	public Vector4 vectorValue;
	public Texture textureValue;
}

[System.Serializable]
public class MaterialStyle {
	public MaterialSetting[] settings;
	public MaterialPropertyBlock materialBlock;

	public void Init() {
		materialBlock = new MaterialPropertyBlock();

		foreach ( MaterialSetting s in settings ) {
			switch ( s.kind ) {
				case MaterialSetting.Kind.Color:
					materialBlock.SetColor( s.property, s.colorValue );
					break;
				case MaterialSetting.Kind.Float:
					materialBlock.SetFloat( s.property, s.floatValue );
					break;
				case MaterialSetting.Kind.Vector:
					materialBlock.SetVector( s.property, s.vectorValue );
					break;
				case MaterialSetting.Kind.Texture:
					materialBlock.SetTexture( s.property, s.textureValue );
					break;
			}
		}
	}

	public void Apply( Renderer r ) {
		r.SetPropertyBlock( materialBlock );
	}
}

[System.Serializable]
public class ButtonMaterialStyle {
	public MaterialStyle defaultStyle;
	public MaterialStyle rolloverStyle;
	public MaterialStyle activatedStyle;

	Renderer renderer;

	public void AssignRenderer( Renderer r ) {
		renderer = r;
		defaultStyle.Init();
		rolloverStyle.Init();
		activatedStyle.Init();
		Reset();
	}

	public void Reset() {
		defaultStyle.Apply( renderer );
	}

	public void Rollover() {
		rolloverStyle.Apply( renderer );
	}

	public void Activate() {
		activatedStyle.Apply( renderer );
	}

}
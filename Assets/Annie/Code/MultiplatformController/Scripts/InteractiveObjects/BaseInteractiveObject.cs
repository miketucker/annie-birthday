﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class BaseInteractiveObject : MonoBehaviour {
    public bool autoEnableInteractivity = true;
	public HandPickupInteractivity AttachedHandInteractivity;
	public float throwVelocityMultiplier = 1f;

	protected Collider[] Colliders;
	protected bool isInteractivityEnabled;
	protected bool isKinematicByDefault = false;
	protected FixedJoint joint;
	protected Transform originalParentTransform;

	// Mappings to quickly find script references based on Unity physics events
	private static Dictionary<Collider, BaseInteractiveObject> colliderMapping;
	private static Dictionary<BaseInteractiveObject, Collider[]> interactiveMapping;

	#region Actions
	public System.Action<HandPickupInteractivity> OnHandPickedUp;
	public System.Action OnHandDropped;                             // does not return HPI because can ever only be held by one hand (TODO?)

	public System.Action<HandPickupInteractivity> OnHandRollOver;
	public System.Action<HandPickupInteractivity> OnHandRollOut; 
	#endregion


	protected virtual void Awake() {
		Colliders = this.GetComponentsInChildren<Collider>();
	}

	protected virtual void Start() {
		RegisterObject( this, new Collider[] { this.GetComponent<Collider>() } );
		if ( autoEnableInteractivity )
			EnableInteractivity();
	}

	public virtual void EnableInteractivity() {
		isInteractivityEnabled = true;
	}

	public virtual void DisableInteractivity() {
		isInteractivityEnabled = false;
	}

    protected virtual void OnDestroy() {
		DeregisterObject( this , Colliders );
        isInteractivityEnabled = true;
    }

	public void Pulse( int microSecondDuration ) {
		if ( AttachedHandInteractivity != null )
			AttachedHandInteractivity.Pulse( microSecondDuration );
	}

	public virtual bool BeginInteraction( HandPickupInteractivity hpi ) {
		if ( AttachedHandInteractivity != null ) {
			Debug.Log( "already picked up!" );
			return false;
		} else {
			originalParentTransform = transform.parent;
			AttachedHandInteractivity = hpi;
			hpi.Pulse( 2000 );
			Rigidbody rb = transform.GetComponent<Rigidbody>();
			isKinematicByDefault = rb.isKinematic;

			rb.isKinematic = false;
			originalParentTransform = transform.parent;
			transform.parent = null;

			joint = gameObject.AddComponent<FixedJoint>();
			//joint.autoConfigureConnectedAnchor = false;
			joint.connectedBody = hpi.rigidBody;
			//joint.anchor = Vector3.zero;

			if ( OnHandPickedUp != null )
				OnHandPickedUp(hpi);

			return true;
		}
	}

	public virtual void EndInteraction(HandPickupInteractivity hpi = null) {


		// remove joint
		DestroyImmediate( joint );
		joint = null;




		// apply throw
		var ahi = AttachedHandInteractivity;
		Transform origin = ahi.transform;
		Rigidbody rb = GetComponent<Rigidbody>();
		rb.isKinematic = isKinematicByDefault;

		transform.parent = originalParentTransform;

		//var _controller = new GameObject();
		//var _controllerRigidBody = _controller.AddComponent<Rigidbody>();
		//_controller.transform.position = ahi.Hand.transform.position;
		//_controller.transform.rotation = ahi.Hand.transform.rotation;
		//_controllerRigidBody.isKinematic = true;
		//_controllerRigidBody.velocity = ahi.Hand.Velocity;
		//_controllerRigidBody.angularVelocity = ahi.Hand.AngularVelocity;

		//rb.velocity = _controllerRigidBody.GetPointVelocity( _controller.transform.TransformPoint( transform.position ) );
		rb.velocity = ahi.Hand.Velocity * throwVelocityMultiplier;// transform.TransformVector( ahi.hand.Velocity );
		rb.angularVelocity = ahi.Hand.AngularVelocity;// transform.TransformVector( ahi.hand.AngularVelocity );

		//Destroy( _controller );
		//Debug.Break();


		// clear
		AttachedHandInteractivity = null;

		if ( OnHandDropped != null )
			OnHandDropped();

	}

	// HandController calls these 

	public virtual void OnRollOver( HandPickupInteractivity hpi ) {
		if ( OnHandRollOver != null )
			OnHandRollOver( hpi );
	}

	public virtual void OnRollOut( HandPickupInteractivity hpi ) {
		if ( OnHandRollOut != null )
			OnHandRollOut( hpi );
	}

	protected virtual void OnCollisionEnter( Collision c ) {

	}

	protected virtual void OnCollisionExit( Collision c ) {

	}


	protected static void RegisterObject( BaseInteractiveObject obj, Collider[] colliders ) {

		//Debug.Log( "register object " + obj.name + " " + colliders.Length );
		InteractiveMapping.Add( obj, colliders );
        for (int i = 0; i < colliders.Length; i++)
        {
            ColliderMapping.Add(colliders[i], obj);
        }

        
	}

	protected static void DeregisterObject( BaseInteractiveObject obj, Collider[] colliders ) {
		//Debug.Log( "deregister object " + obj.name + " " + colliders.Length );
		// todo: deregister from hands if its being held..

		InteractiveMapping.Remove( obj );
		ColliderMapping = ColliderMapping.Where( mapping => mapping.Value != obj ).ToDictionary( mapping => mapping.Key, mapping => mapping.Value );
	}

	public static BaseInteractiveObject FindInteractable( Collider c ) {
		BaseInteractiveObject interactable;
		ColliderMapping.TryGetValue( c, out interactable );
		return interactable;
	}


	static Dictionary<Collider, BaseInteractiveObject> ColliderMapping
	{
		get
		{
			if ( colliderMapping == null )
				colliderMapping = new Dictionary<Collider, BaseInteractiveObject>();
			return colliderMapping;
		}
		set
		{
			colliderMapping = value;
		}
	}

	static Dictionary<BaseInteractiveObject, Collider[]> InteractiveMapping
	{
		get
		{
			if ( interactiveMapping == null )
				interactiveMapping = new Dictionary<BaseInteractiveObject, Collider[]>();
			return interactiveMapping;
		}
	}

	public bool isPickedUp {
		get { return AttachedHandInteractivity != null ; }
	}

	public Vector3 Position
	{
		get { return transform.position; }
	}

}

﻿using UnityEngine;
using System.Collections;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

[CustomPropertyDrawer(typeof(RangeFloat))]
public class RangeFloatDrawer : PropertyDrawer
{
    const int boxWidth = 40;
    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        SerializedProperty minProp = prop.FindPropertyRelative("min");
        SerializedProperty maxProp = prop.FindPropertyRelative("max");


        GUIStyle style = GUI.skin.GetStyle("label");
        style.alignment = TextAnchor.LowerRight;
        float propWidth = position.width / 6.0f;
        // Draw scale
        GUI.skin.label.alignment = TextAnchor.LowerRight;

        float propOffset = 0f;

        EditorGUI.LabelField(new Rect(position.x, position.y, propWidth * 3, position.height), prop.name + " Min", style);
        propOffset += propWidth * 3;
        minProp.floatValue = EditorGUI.FloatField(new Rect(position.x + propOffset, position.y, propWidth, position.height), minProp.floatValue);
        propOffset += propWidth;
        EditorGUI.LabelField(new Rect(position.x + propOffset, position.y, propWidth, position.height), " Max", style);
        propOffset += propWidth;
        maxProp.floatValue = EditorGUI.FloatField(new Rect(position.x + propOffset, position.y, propWidth, position.height), maxProp.floatValue);
        //pos.x += pos.width;

        //maxProp.floatValue = EditorGUI.FloatField(new Rect(pos.x, pos.y, pos.width - boxWidth, pos.height), maxProp.floatValue);

        // Draw curve
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = indent;
    }
}




[CustomPropertyDrawer(typeof(RangeInt))]
public class RangeIntDrawer : PropertyDrawer
{
    const int boxWidth = 40;
    public override void OnGUI(Rect position, SerializedProperty prop, GUIContent label)
    {
        SerializedProperty minProp = prop.FindPropertyRelative("min");
        SerializedProperty maxProp = prop.FindPropertyRelative("max");


        GUIStyle style = GUI.skin.GetStyle("label");
        style.alignment = TextAnchor.LowerRight;
        float propWidth = position.width / 6.0f;
        // Draw scale
        GUI.skin.label.alignment = TextAnchor.LowerRight;

        float propOffset = 0f;

        EditorGUI.LabelField(new Rect(position.x, position.y, propWidth * 3, position.height), prop.name + " Min", style);
        propOffset += propWidth * 3;
        minProp.intValue = EditorGUI.IntField(new Rect(position.x + propOffset, position.y, propWidth, position.height), minProp.intValue);
        propOffset += propWidth;
        EditorGUI.LabelField(new Rect(position.x + propOffset, position.y, propWidth, position.height), " Max", style);
        propOffset += propWidth;
        maxProp.intValue = EditorGUI.IntField(new Rect(position.x + propOffset, position.y, propWidth, position.height), maxProp.intValue);
        //pos.x += pos.width;

        //maxProp.floatValue = EditorGUI.FloatField(new Rect(pos.x, pos.y, pos.width - boxWidth, pos.height), maxProp.floatValue);

        // Draw curve
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = indent;
    }
}

#endif

[System.Serializable]
public class RangeFloat
{
    public float min;
    public float max;

    public RangeFloat(float min, float max)
    {
        this.min = min;
        this.max = max;
    }

    public float random
    {
        get { return Random.Range(min, max); }
    }

    public float range
    {
        get { return max - min; }
    }

    public float Percent(float f)
    {
        return f * range + min;
    }

    public float PointInRange(float f)
    {
        return Mathf.Clamp01((f - min) / range);
    }
}



[System.Serializable]
public class RangeInt
{
    public int min;
    public int max;

    public RangeInt(int min, int max)
    {
        this.min = min;
        this.max = max;
    }

    public int random
    {
        get { return Mathf.FloorToInt(Random.Range(min, max + 1)); }
    }

    public int range
    {
        get { return max - min; }
    }

    public int Percent(float f)
    {
        return Mathf.RoundToInt(f * range + min);
    }

    public bool Contains(int n)
    {
        return (n >= min && n <= max);
    }
}
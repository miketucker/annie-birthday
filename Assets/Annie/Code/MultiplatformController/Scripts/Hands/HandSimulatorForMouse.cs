﻿using UnityEngine;
using System.Collections;

public class HandSimulatorForMouse : BaseHandBinding {

	public float handDistanceFromCamera = 0.1f;
	public bool useMouseWheelToChangeDistance = true;
	
	// not currently working
	public bool useRaycastToAutoChangeDistance = true;

	public float mouseWheelSpeed = 0.01f;
	float smoothingSpeed = 5f;
	public Collider handCollider;
	Camera cam;

	private LayerMask handSimulatorLayerMask;
	private LayerMask controllerCollidersLayerMask;



	// Use this for initialization
	void Start () {
		targetHandDistanceFromCamera = handDistanceFromCamera;
		cam = Camera.main;
		if(cam == null ) {
			Camera[] cams = FindObjectsOfType<Camera>();
			foreach ( var c in cams ) {
				if ( c.enabled ) {
					cam = c;
					break;
				}
			}
		}
		if(cam == null ) {
			// Debug.Log( "could not find active cam, disabling" );
			enabled = false;
		}

		handSimulatorLayerMask = ( 1 << LayerMask.NameToLayer("HandSimulator") );
		controllerCollidersLayerMask = ( 1 << LayerMask.NameToLayer("ControllerColliders") );
	}

	public override Vector3 AngularVelocity
	{
		// Not yet implemented
		get { return Vector3.zero; }
	}


	public override Vector3 Velocity {
		get { return (transform.position - lastPosition) * (1f / Time.deltaTime); }
	}

	float targetHandDistanceFromCamera;

	// Update is called once per frame
	void Update () {

		if ( Input.GetMouseButtonDown( 0 ) ) {
			buttonState.triggerPressed = true;
			buttonState.triggerValue = 1f;

			if ( useRaycastToAutoChangeDistance ) {
				
				Ray mouseRay = cam.ScreenPointToRay( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 0.0f ) );
				// Debug.DrawRay(mouseRay.origin, 10.0f * mouseRay.direction, Color.yellow);
				RaycastHit[] hits = Physics.RaycastAll( mouseRay, 100.0f, ~( handSimulatorLayerMask | controllerCollidersLayerMask ) );
				foreach ( RaycastHit hit in hits ) {
					// Debug.Log( "hit " + hit.transform.name + " " + hit.transform.parent.name );
					if ( hit.collider != null && hit.transform.parent != transform ) {
						Plane camPlane = new Plane(cam.transform.forward.normalized, cam.transform.position);
						float dist = Mathf.Abs( camPlane.GetDistanceToPoint( hit.transform.position ) );
						handDistanceFromCamera = dist;
						targetHandDistanceFromCamera = dist;
						// Debug.Log( "using " + hit.transform.name );
						break;
					}
				}
			}
		}

		if ( Input.GetMouseButtonUp( 0 ) ) {
			buttonState.triggerPressed = false;
			buttonState.triggerValue = 0f;
		}

		if ( Input.GetMouseButtonDown( 1 ) ) {
			buttonState.menuPressed = true;
		}

		if ( Input.GetMouseButtonUp( 1 ) ) {
			buttonState.menuPressed = false;
		}


		if (useMouseWheelToChangeDistance) {
			targetHandDistanceFromCamera = Mathf.Clamp (targetHandDistanceFromCamera + Input.GetAxis ("Mouse ScrollWheel"), 0.15f, 2f);
		}


		handDistanceFromCamera += ( targetHandDistanceFromCamera - handDistanceFromCamera ) * ( Time.deltaTime * smoothingSpeed );

		Vector3 p = cam.ScreenToWorldPoint (new Vector3(Input.mousePosition.x, Input.mousePosition.y, handDistanceFromCamera));
		transform.position = p;
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class HandPickupInteractivity : MonoBehaviour {

	public Rigidbody rigidBody;
	HandController _hand;
	HashSet<BaseInteractiveObject> CurrentlyHoveringOver = new HashSet<BaseInteractiveObject>();



	float pickupRadius = 0.1f;
	BaseInteractiveObject _closestObject = null;
	BaseInteractiveObject _currentRollover = null;

	public System.Action<int, float> SendPulseEvent;

	public BaseInteractiveObject HeldObject { private set; get; }

	public HandController Hand {
		get { return _hand; }
	}

	// Use this for initialization
	void Start() {
		var sc = gameObject.AddComponent<SphereCollider>();
		sc.radius = pickupRadius;
		sc.isTrigger = true;

		rigidBody = gameObject.AddComponent<Rigidbody>();
		rigidBody.isKinematic = true;
	}

	public void Register(HandController hand) {
		this._hand = hand;
		hand.TriggerUnclicked += OnTriggerUnclick;
		hand.TriggerClicked += OnTriggerClick;
	}

	void OnDisable() {
		if (_hand != null) {
			_hand.TriggerUnclicked -= OnTriggerUnclick;
			_hand.TriggerClicked -= OnTriggerClick;
		}
	}

	private void OnTriggerUnclick(StandardButtonState buttonState) {
		Drop();
	}

	void OnTriggerClick(StandardButtonState buttonState) {
		Pickup();
	}


	public void Pickup() {

		if (HeldObject != null) {
			Debug.Log("Already interacting, no pickup");
			return;
		}

		if (_currentRollover != null) {
			_closestObject = _currentRollover;
		}
		else {
			_closestObject = CalculateClosestObject();
		}

		if (_closestObject != null) {
			BeginInteraction(_closestObject);
		}
	}

	BaseInteractiveObject CalculateClosestObject() {
		float closestDistance = float.MaxValue;
		BaseInteractiveObject closestObject = null;
		Vector3 pos = this.transform.position;

		foreach (var hovering in CurrentlyHoveringOver) {
			if (hovering == null)
				continue;

			float sqrDist = (pos - hovering.Position).sqrMagnitude;
			if (sqrDist < closestDistance) {
				closestDistance = sqrDist;
				closestObject = hovering;
			}
		}
		return closestObject;
	}

	public void Drop() {
		EndInteraction();
	}

	protected void BeginInteraction(BaseInteractiveObject obj) {

		if (obj.BeginInteraction(this)) {
			HeldObject = obj;

			if (HeldObject == _currentRollover) {
				_currentRollover.OnRollOut(this);
			}
		}
	}

	public void Pulse(int microsecondDuration, float magnitude = 1f) {
		if (SendPulseEvent != null)
			SendPulseEvent(microsecondDuration, magnitude);
	}

	protected void EndInteraction() {
		if (HeldObject != null) {
			HeldObject.EndInteraction(this);
			HeldObject = null;
		}
	}

	#region Triggers


	private void OnTriggerStay(Collider collider) {
		BaseInteractiveObject interactiveObj = BaseInteractiveObject.FindInteractable(collider);
		if (interactiveObj == null || interactiveObj.enabled == false)
			return;

		if (!CurrentlyHoveringOver.Contains(interactiveObj)) CurrentlyHoveringOver.Add(interactiveObj);

		// Do rollover events if there is no object currently being held
		if (HeldObject == null) {
			BaseInteractiveObject newClosestObject = CalculateClosestObject();
			// Do not repeat rollover event if the closest is the same as previous frame
			if (newClosestObject != null && (newClosestObject != _currentRollover)) {
				// If we were rolling over something else, roll out of it
				if (_currentRollover != null)
					_currentRollover.OnRollOut(this);
				_currentRollover = newClosestObject;
				_currentRollover.OnRollOver(this);
			}
		}

	}

	void OnTriggerExit(Collider collider) {
		BaseInteractiveObject interactiveObj = BaseInteractiveObject.FindInteractable(collider);
		if (interactiveObj != null) {
			CurrentlyHoveringOver.Remove(interactiveObj);
			if (_currentRollover == interactiveObj) {
				_currentRollover.OnRollOut(this);
				_currentRollover = null;
			}
		}
	}

	#endregion


	public float PickupRadius {
		get { return pickupRadius; }
		set {
			pickupRadius = value;
			GetComponent<SphereCollider>().radius = pickupRadius;
		}
	}
}

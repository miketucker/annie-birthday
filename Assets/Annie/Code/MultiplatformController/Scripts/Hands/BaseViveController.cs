﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SteamVR_TrackedObject))]
public class BaseViveController : BaseHandBinding
{
    public uint controllerIndex = 999;
	public Valve.VR.VRControllerState_t controllerState;
    public bool showDebugValues = false;
    public Rect debugRect = new Rect(10, 10, 200, 200);


    SteamVR_TrackedObject trackedObj;

    // Use this for initialization
    protected virtual void OnEnable()
    {
        trackedObj = GetComponent<SteamVR_TrackedObject>();
       
    }

    protected virtual void OnDisable()
    {

    }
    

    void OnGUI()
    {
        if (showDebugValues)
        {
            var device = SteamVR_Controller.Input((int)trackedObj.index);
            GUILayout.BeginArea(debugRect);
            GUILayout.Label("Device ID: " + trackedObj.index);
            GUILayout.Label("Trigger: " + device.GetTouch(SteamVR_Controller.ButtonMask.Trigger));
            GUILayout.Label("Grip: " + device.GetTouch(SteamVR_Controller.ButtonMask.Grip));
            GUILayout.Label("System: " + device.GetTouch(SteamVR_Controller.ButtonMask.System));
            GUILayout.Label("ApplicationMenu: " + device.GetTouch(SteamVR_Controller.ButtonMask.ApplicationMenu));
            GUILayout.Label("TouchPad: " + device.GetTouch(SteamVR_Controller.ButtonMask.Touchpad));
            GUILayout.Label("Trigger axis?: " + device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger));
            GUILayout.Label("Axis: " + device.GetAxis());
            GUILayout.EndArea();
            //			GUILayout.Label ("Axis0: " + device.GetTouch(SteamVR_Controller.ButtonMask.Axis0));

        }

    }

	public override void Pulse( int microsecondDuration , float magnitude = 1f) {
		SteamVR_Controller.Input( (int) trackedObj.index ).TriggerHapticPulse( (ushort) microsecondDuration);
	}

	public override Vector3 Velocity
	{
		get
		{
			var device = SteamVR_Controller.Input( (int) trackedObj.index );
			return device.velocity;
		}
	}

	public override Vector3 AngularVelocity
	{
		get
		{
			var device = SteamVR_Controller.Input( (int) trackedObj.index );
			return device.angularVelocity;
		}
	}

	// Update is called once per frame
	protected virtual void Update()
    {
        var device = SteamVR_Controller.Input((int)trackedObj.index);
        controllerIndex = device.index;
        //var tst = device.index;//controllerIndex
        //Debug.L
        if (SteamVR.instance.hmd.GetControllerState(controllerIndex, ref controllerState)) 
        {
			buttonState.triggerValue = device.GetAxis(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger).x;
			buttonState.padPosition = device.GetAxis();
			buttonState.triggerValue = device.GetAxis( Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger ).x;
			buttonState.padPosition = device.GetAxis();


            ulong trigger = controllerState.ulButtonPressed & (1UL << ((int)Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger));
            if (trigger > 0L && !buttonState.triggerPressed )
            {
				buttonState.triggerPressed = true;

            }
            else if (trigger == 0L && buttonState.triggerPressed )
            {
				buttonState.triggerPressed = false;

            }

            ulong grip = controllerState.ulButtonPressed & (1UL << ((int)Valve.VR.EVRButtonId.k_EButton_Grip));
            if (grip > 0L && !buttonState.gripped )
            {
				buttonState.gripped = true;

            }
            else if (grip == 0L && buttonState.gripped )
            {
				buttonState.gripped = false;
            }

            ulong pad = controllerState.ulButtonPressed & (1UL << ((int)Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));
            if (pad > 0L && !buttonState.padPressed )
            {
				buttonState.padPressed = true;
            }
            else if (pad == 0L && buttonState.padPressed )
            {
				buttonState.padPressed = false;
            }

            ulong menu = controllerState.ulButtonPressed & (1UL << ((int)Valve.VR.EVRButtonId.k_EButton_ApplicationMenu));
            if (menu > 0L && !buttonState.menuPressed )
            {
				buttonState.menuPressed = true;
            }
            else if (menu == 0L && buttonState.menuPressed )
            {
				buttonState.menuPressed = false;
            }

			// Note: As of 4/10/16 the Home/Steam button does not work, and falsely triggers the Grip button
			//ulong home = controllerState.ulButtonPressed & ( 1UL << ( (int) Valve.VR.EVRButtonId.k_EButton_Dashboard_Back ) );
			//if ( home > 0L && !buttonState.homePressed ) {
			//	buttonState.homePressed = true;
			//} else if ( home == 0L && buttonState.homePressed ) {
			//	buttonState.homePressed = false;
			//}

			pad = controllerState.ulButtonTouched & (1UL << ((int)Valve.VR.EVRButtonId.k_EButton_SteamVR_Touchpad));
            if (pad > 0L && !buttonState.padTouched )
            {
				buttonState.padTouched = true;

            }
            else if (pad == 0L && buttonState.padTouched )
            {
				buttonState.padTouched = false;
            }
        }
    }
}

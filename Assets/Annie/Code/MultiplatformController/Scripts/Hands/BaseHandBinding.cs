﻿using UnityEngine;
using System.Collections;
using System;

public class BaseHandBinding : MonoBehaviour {
	public StandardButtonState buttonState = new StandardButtonState();

	// To calculate physics
	protected Vector3 currentPosition;
	protected Vector3 lastPosition;
	protected Quaternion currentRotation;
	protected Quaternion lastRotation;

	public float velocityMultiplier = 1f;

	protected virtual void FixedUpdate() {
		lastPosition = currentPosition;
		currentPosition = transform.position;

		lastRotation = currentRotation;
		currentRotation = transform.rotation;
	}


	public virtual Vector3 AngularVelocity
	{
		get { return Vector3.zero; }
	}

	public virtual Vector3 Velocity
	{
		get { return velocityMultiplier * (transform.position - lastPosition); }
	}

	public virtual void Pulse( int millisecondDuration, float magnitude ) {
		// for override
	}
}

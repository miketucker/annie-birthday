﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using SixenseCore;
using System;

[System.Serializable]
public class StandardButtonState {

	public enum TouchpadDirection {
		Left, Right, Up, Down
	}

	public bool triggerPressed = false;
	public bool homePressed = false;
	public bool menuPressed = false;
	public bool padPressed = false;
	public bool padTouched = false;
	public bool gripped = false;

	public float triggerValue = 0f;							// 0 to 1
	public Vector2 padPosition = Vector2.zero;			// radial, -1 to 1


	public void CopyTo(StandardButtonState otherState ) {
		otherState.triggerPressed = triggerPressed;
		otherState.homePressed = homePressed;
		otherState.menuPressed = menuPressed;
		otherState.padPressed = padPressed;
		otherState.padTouched = padTouched;
		otherState.gripped = gripped;
		otherState.triggerValue = triggerValue;
		otherState.padPosition = padPosition;

	}


	public TouchpadDirection touchpadDirection
	{
		get {
			Vector2 p = padPosition;

			float absX = Mathf.Abs( p.x );
			float absY = Mathf.Abs( p.y );

			if ( absX > absY ) {
				if ( p.x < 0f ) {
					return TouchpadDirection.Left;
				} else {
					return TouchpadDirection.Right;
				}
			} else {
				if ( p.y < 0f ) {
					return TouchpadDirection.Down;
				} else {
					return TouchpadDirection.Up;
				}
			}
		}
	}
}

public class HandController : MonoBehaviour {

	public enum ControllerType {
		Unassigned,
		Stem,
		Vive,
		Mouse
	}

	public bool addPickupInteractivity = true;
	public StandardButtonState buttonState = new StandardButtonState();

	public HandPickupInteractivity handPickupInteractivity { private set; get; }

	public ControllerType controllerType = ControllerType.Unassigned;
	public static List<HandController> Hands = new List<HandController>( 10 );

	// Event called when a controller is turned on or off
	public static Action<HandController> HandRegistered;
	public static Action<HandController> HandUnregistered;

	#region Button Events
	public Action<StandardButtonState> MenuButtonClicked;
	public Action<StandardButtonState> MenuButtonUnclicked;
	public Action<StandardButtonState> TriggerClicked;
	public Action<StandardButtonState> TriggerUnclicked;
	public Action<StandardButtonState> HomeClicked;
	public Action<StandardButtonState> HomeUnclicked;
	public Action<StandardButtonState> PadClicked;
	public Action<StandardButtonState> PadUnclicked;
	public Action<StandardButtonState> PadTouched;
	public Action<StandardButtonState> PadUntouched;
	public Action<StandardButtonState> Gripped;
	public Action<StandardButtonState> Ungripped; 
	#endregion

	BaseHandBinding binding;

	static int nextHandIndex = 0;								// hand index increments with each connection and is never modified. so its possible to have a 1 and 2 but not a 0 if the first disconnects
	public int HandIndex { private set; get; }

	public Vector3 Position
	{
		get { return transform.position; }
	}

	// Update is called once per frame
	void OnEnable() {
		if ( !Hands.Contains( this ) ) {
			HandIndex = nextHandIndex;
			nextHandIndex++;
			Hands.Add( this );
			
			if ( HandRegistered != null )
				HandRegistered( this );
			Debug.Log( "Hand registered: " + gameObject.name );
		} else {
			Debug.LogError( gameObject.name + " : hand already exists" );
		}
	}

	void OnDisable() {
		if ( Hands.Contains( this ) ) {
			Hands.Remove( this );
			if ( HandUnregistered != null )
				HandUnregistered( this );

			Debug.Log( "Unregistered hand: " + gameObject.name );
		}
	}

	void Start() {
		if ( addPickupInteractivity ) {
			GameObject g = new GameObject( "PickupInteractivity" );
			handPickupInteractivity = g.AddComponent<HandPickupInteractivity>();
			//if(Application.ta)
			//g.tag = "Hand";
			handPickupInteractivity.Register( this );
			handPickupInteractivity.SendPulseEvent += Pulse;
			g.transform.parent = transform;
			g.transform.localPosition = Vector3.zero;
		}

	}

	public void Pulse( int microsecondDuration, float magnitude = 1f ) {
		if ( binding != null ) {
			binding.Pulse( microsecondDuration, magnitude );
		}
	}

	void LateUpdate() {
		switch ( controllerType ) {
			case ControllerType.Unassigned:
				CheckForController();
					break;
			default:
				DetectChangesInButtonState();
				break;

		}
	}
	


	#region ButtonEvents

	void DetectChangesInButtonState( ) {
		if(binding == null ) {
			controllerType = ControllerType.Unassigned;
			return;
		}
		StandardButtonState otherButtonState = binding.buttonState;

		// Trigger

		if ( otherButtonState.triggerPressed != buttonState.triggerPressed ) {
			buttonState.triggerPressed = otherButtonState.triggerPressed;

			if ( buttonState.triggerPressed ) {
				if ( TriggerClicked != null )
					TriggerClicked( buttonState );
			} else {
				if ( TriggerClicked != null )
					TriggerUnclicked( buttonState );
			}
		}

		// Menu

		if ( otherButtonState.menuPressed != buttonState.menuPressed ) {
			buttonState.menuPressed = otherButtonState.menuPressed;

			if ( buttonState.menuPressed ) {
				if ( MenuButtonClicked != null )
					MenuButtonClicked( buttonState );
			} else {
				if ( MenuButtonUnclicked != null )
					MenuButtonUnclicked( buttonState );
			}
		}

		// Grip

		if ( otherButtonState.gripped != buttonState.gripped ) {
			buttonState.gripped = otherButtonState.gripped;

			if ( buttonState.gripped ) {
				if ( Gripped != null )
					Gripped( buttonState );
			} else {
				if ( Ungripped != null )
					Ungripped( buttonState );
			}
		}

		// Home

		if ( otherButtonState.homePressed != buttonState.homePressed ) {
			buttonState.homePressed = otherButtonState.homePressed;

			if ( buttonState.homePressed ) {
				if ( HomeClicked != null )
					HomeClicked( buttonState );
			} else {
				if ( HomeUnclicked != null )
					HomeUnclicked( buttonState );
			}
		}


		// Touch Pad

		if ( otherButtonState.padPosition != buttonState.padPosition ) {
			buttonState.padPosition = otherButtonState.padPosition;
			if ( PadTouched != null )
				PadTouched( buttonState );

		}

		if ( otherButtonState.padTouched != buttonState.padTouched ) {
			buttonState.padTouched = otherButtonState.padTouched;
			if ( PadTouched != null )
				PadTouched( buttonState );
		}

		if ( otherButtonState.padPressed != buttonState.padPressed ) {
			buttonState.padPressed = otherButtonState.padPressed;
			if ( buttonState.padPressed ) {
				if ( PadClicked != null )
					PadClicked( buttonState );
			} else {
				if ( PadClicked != null )
					PadClicked( buttonState );
			}
		}

		otherButtonState.CopyTo( buttonState );
	}

	#endregion

	#region HandPhysics

	public Vector3 Velocity
	{
		get
		{
			if ( binding != null )
				return binding.Velocity;
			else
				return Vector3.zero;
		}
	}

	public Vector3 AngularVelocity
	{
		get
		{
			if ( binding != null ) 
				return binding.AngularVelocity;
			else 
			return Vector3.zero;
		}
	}

	#endregion

	#region ControllerBindings

	void CheckForController() {

		//StemController stem = GetComponent<StemController>();
		//if ( stem != null ) {
		//	controllerType = ControllerType.Stem;
		//	binding = GetComponent<StemController>();

		//	return;
		//}

		BaseViveController baseVive = GetComponent<BaseViveController>();
		if ( baseVive != null ) {
			controllerType = ControllerType.Vive;
			binding = baseVive;
			return;
		}

		HandSimulatorForMouse handSimulatorForMouse = GetComponent<HandSimulatorForMouse>();
		if ( handSimulatorForMouse != null ) {
			controllerType = ControllerType.Mouse;
			binding = handSimulatorForMouse;
			return;
		}
	}


	#endregion
}	

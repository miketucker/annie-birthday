﻿using UnityEngine;
using System.Collections;

public class MouseRotateCam : MonoBehaviour {
	public float rotateInfluence = 25f;
	public bool onlyUsedInEditor = true;
	public KeyCode holdDownKeyToEnable = KeyCode.None;
	public bool holdRightClickToEnable = false;
	// Update is called once per frame
	void Update () {
		if ((Application.isEditor || !onlyUsedInEditor) && (holdDownKeyToEnable == KeyCode.None || Input.GetKey(holdDownKeyToEnable) ) && ((holdRightClickToEnable && Input.GetMouseButton(1)) || !holdRightClickToEnable) ) {
			Vector3 euler = transform.localEulerAngles;
			euler.y += Input.GetAxis ("Mouse X") * (rotateInfluence * 3.25f * Time.deltaTime);
			euler.x -= Input.GetAxis ("Mouse Y") * (rotateInfluence * 2.25f * Time.deltaTime);
			transform.localEulerAngles = euler;
		}
	}
}

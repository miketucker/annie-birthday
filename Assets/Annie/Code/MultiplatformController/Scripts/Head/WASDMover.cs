﻿using UnityEngine;
using System.Collections;

public class WASDMover : MonoBehaviour {
	public bool onlyInEditor = true;
	public float verticalMoveSpeed = 0.5f;
	public float moveSpeed = 1f;
	Vector3 moveDelta;

	
	// Update is called once per frame
	void Update () {

		if (Application.isEditor || !onlyInEditor) {

			moveDelta = Vector3.zero;

			//Camera translation
			if (Input.GetKey (KeyCode.Q))
				moveDelta += (transform.up * (verticalMoveSpeed  * Time.deltaTime));
		
			if (Input.GetKey (KeyCode.E))
				moveDelta += (transform.up * (-verticalMoveSpeed  * Time.deltaTime));
		
			if (Input.GetKey (KeyCode.A))
				moveDelta += (transform.right * (-moveSpeed  * Time.deltaTime));
							
			if (Input.GetKey (KeyCode.D))
				moveDelta += (transform.right * (moveSpeed  * Time.deltaTime));

			if (Input.GetKey (KeyCode.W))
				moveDelta += (transform.forward * (moveSpeed  * Time.deltaTime));
		
			if (Input.GetKey (KeyCode.S))
				moveDelta += (transform.forward * (-moveSpeed  * Time.deltaTime));
			
			transform.position += moveDelta;
		}
	
	}

}

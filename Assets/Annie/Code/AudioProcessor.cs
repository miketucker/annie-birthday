﻿using UnityEngine;
using System.Collections;
//using TBE_3DCore;
public class AudioProcessor : MonoBehaviour {

	public enum WindowSize {
		_64 = 64,
		_128 = 128,
		_256 = 256,
		_512 = 512,
		_1024 = 1024,
		_2048 = 2048,
		_4096 = 4096,
		_8192 = 8192
	}

	public enum Channel {
		Left,
		Right,
		Average
	}

	public enum SpectrumType {
		Spectrum,
		SmoothedSpectrum,
		LogSpectrum,
		LogSmoothedSpectrum
	}

	[System.Serializable]
	public class FreqRange {
		public string _name;
		public Color _debugColor = Color.blue;
		public SpectrumType _type = SpectrumType.SmoothedSpectrum;
		[Range (0f, 22050f)]
		public float _minHz = 0f;
		[Range (0f, 22050f)]
		public float _maxHz = 1000f;
		public float _scaling = 100f;
		public float _average, _min, _max;
	}

	public bool _calculateSpectrum = true;
	public bool _drawDebug = false;
	public bool _drawGizmos = false;
	public SpectrumType _debugSpectrum = SpectrumType.LogSmoothedSpectrum;

	public Channel _channel;
	public WindowSize _windowSize = WindowSize._2048;

	public FFTWindow _windowType = FFTWindow.Hamming;

	[Range (0f, 1f)]
	public float _smoothSpectrumScaleFactor = .95f;

	public bool _useDefaultFreqRanges = false;
	public FreqRange[] _freqRanges;
	
    public AudioSource _audioSource;
	float _frequencyResolution = 0f;
	float _noiseFloordB = -20f;

	public bool _compressDynamicRange = false;
	public AnimationCurve _compressionCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

	public float[] _rawData;
 	public float[] _spectrumData;
 	public float[] _logSpectrumData;
	public float[] _smoothSpectrumData;
	public float[] _logSmoothSpectrumData;

	public bool _isPaused = false;
	public bool _isInited = false;
	float[] _auxillaryData;

	// Use this for initialization
	void Start () {

        ResetData();
		Init();

	}

	void Init() {
		int samplingRate = (_audioSource && _audioSource.clip) ? _audioSource.clip.frequency : 44000;
		_frequencyResolution = samplingRate / (int)_windowSize;

        if (_useDefaultFreqRanges) {
        	_freqRanges = new FreqRange[] {
				new FreqRange(){_name = "Bass", _minHz = 0, _maxHz = 300, _debugColor = Color.blue}, 
				new FreqRange(){_name = "Mid", _minHz = 300, _maxHz = 1000, _debugColor = Color.green},
				new FreqRange(){_name = "Treble", _minHz = 1000, _maxHz = 5000, _debugColor = Color.red},
				new FreqRange(){_name = "High", _minHz = 5000, _maxHz = 22050, _debugColor = Color.yellow}	
			};
        }

        foreach (FreqRange fr in _freqRanges)
        {
            fr._maxHz = Mathf.Clamp(fr._maxHz, _frequencyResolution, samplingRate / 2);
            fr._minHz = Mathf.Min(fr._minHz, fr._maxHz - _frequencyResolution);
        }

        _isInited = true;
	}
	
	// Update is called once per frame
	void Update () {
		if ( _isPaused )
			return;

		if (!_isInited) Init();

		if (_rawData.Length != (int)_windowSize) {
			ResetData();
		}

		GetAudioData();
		
	}

    public int GetFrequencyIndex(float frequency) {
    	if (_frequencyResolution == 0f)
    		return -1;
    	else
        	return (int)(frequency / _frequencyResolution);
    }

	void ResetData() {
		_rawData = new float[(int)_windowSize];
		_spectrumData = new float[(int)_windowSize];
		_logSpectrumData = new float[(int)_windowSize];
		_smoothSpectrumData = new float[(int)_windowSize];
		_logSmoothSpectrumData = new float[(int)_windowSize];
		_auxillaryData = new float[(int)_windowSize];
	}

	void ReadAudioFromSource() {
		if (_channel == Channel.Left || _channel == Channel.Right) {
			if (_calculateSpectrum)
				_audioSource.GetSpectrumData(_spectrumData, (int)_channel, _windowType);
			_audioSource.GetOutputData(_rawData, (int)_channel);
		}
		else if (_channel == Channel.Average) {
			if (_calculateSpectrum) {
				_audioSource.GetSpectrumData(_spectrumData, 0, _windowType);
				_audioSource.GetSpectrumData(_auxillaryData, 1, _windowType);
				for (int i = 0; i < (int)_windowSize; i++) {
					_spectrumData[i] = (_spectrumData[i] + _auxillaryData[i]) * 0.5f;
				}
			}
			_audioSource.GetOutputData(_rawData, 0);
			_audioSource.GetOutputData(_auxillaryData, 1);
			for (int i = 0; i < (int)_windowSize; i++) {
				_rawData[i] = (_rawData[i] + _auxillaryData[i]) * 0.5f;
			}
		}

		if (_compressDynamicRange) {
			for (int i = 0; i < (int)_windowSize; i++) {
				float sign = Mathf.Sign(_rawData[i]);
				float val = _compressionCurve.Evaluate(Mathf.Abs(_rawData[i]));
				_rawData[i] = sign * val;
			}
		}
	}


	void ReadAudioFromListener() {
		if (_channel == Channel.Left || _channel == Channel.Right) {
			if (_calculateSpectrum)
				AudioListener.GetSpectrumData(_spectrumData, (int)_channel, _windowType);
			AudioListener.GetOutputData(_rawData, (int)_channel);
			
		}
		else if (_channel == Channel.Average) {
			if (_calculateSpectrum) {
				AudioListener.GetSpectrumData(_spectrumData, 0, _windowType);
				AudioListener.GetSpectrumData(_auxillaryData, 1, _windowType);
				for (int i = 0; i < (int)_windowSize; i++) {
					_spectrumData[i] = (_spectrumData[i] + _auxillaryData[i]) * 0.5f;
				}
			}
			AudioListener.GetOutputData(_rawData, 0);
			AudioListener.GetOutputData(_auxillaryData, 1);
			for (int i = 0; i < (int)_windowSize; i++) {
				_rawData[i] = (_rawData[i] + _auxillaryData[i]) * 0.5f;
			}
		}

		if (_compressDynamicRange) {
			for (int i = 0; i < (int)_windowSize; i++) {
				float sign = Mathf.Sign(_rawData[i]);
				float val = _compressionCurve.Evaluate(Mathf.Abs(_rawData[i]));
				_rawData[i] = sign * val;
			}
		}
	}

	void GetAudioData() {
		// Get raw data and spectrum
		if (_audioSource && _audioSource.clip)
			ReadAudioFromSource();
		else
			ReadAudioFromListener();

		// Get log spectrum
		if (_calculateSpectrum) {
			for (int i = 0; i < (int)_windowSize; i++) {
				_logSpectrumData[i] = Mathf.Log10(_spectrumData[i]);
			}
			// Smoothed Spectrums
			for (int i = 0; i < (int)_windowSize; i++){
			    _smoothSpectrumData[i] *= _smoothSpectrumScaleFactor;
			    if (_smoothSpectrumData[i] < _spectrumData[i]) {
			        _smoothSpectrumData[i] = _spectrumData[i];
			    }
			    _logSmoothSpectrumData[i] += Mathf.Log10(_smoothSpectrumScaleFactor);
			    _logSmoothSpectrumData[i] = Mathf.Max(_logSmoothSpectrumData[i], _noiseFloordB);
			    if (_logSmoothSpectrumData[i] < _logSpectrumData[i]) {
			        _logSmoothSpectrumData[i] = _logSpectrumData[i];
			    }
			}

			foreach (FreqRange fr in _freqRanges) {
				int minIndex = (int) (fr._minHz / _frequencyResolution);
				int maxIndex = (int) (fr._maxHz / _frequencyResolution);

				float[] spectrum = GetSpectrum(fr._type);

				float minVal = spectrum[0];
				float maxVal = spectrum[0];
				float avgVal = 0f;
				for (int i = minIndex; i <= maxIndex; i++) {
					if (spectrum[i] < minVal) {
						minVal = spectrum[i];
					}
					if (spectrum[i] > maxVal) {
						maxVal = spectrum[i];
					}
					avgVal = avgVal + spectrum[i];
				}
				avgVal = avgVal / (maxIndex - minIndex);

				if (fr._type == SpectrumType.Spectrum || fr._type == SpectrumType.SmoothedSpectrum) {
					fr._average = avgVal * fr._scaling;
					fr._min = minVal * fr._scaling;
					fr._max = maxVal * fr._scaling;
				} else {
					float logScaling = Mathf.Log10(fr._scaling);
					fr._average = avgVal + logScaling;
					fr._min = minVal + logScaling;
					fr._max = maxVal + logScaling;
				}
			}

			if (_drawDebug) DrawDebugLines();
		}
	}

	void OnValidate() {
		if ( _audioSource == null )
			_audioSource = GetComponent<AudioSource>();
	}

	public float GetRMS() {
		float rms = 0;
		for (int i = 0; i < _rawData.Length; i++) {
			rms += _rawData[i] * _rawData[i];
		}
		rms /= _rawData.Length;

		return Mathf.Sqrt(rms);
	}

	public float GetPeakFrequency(float thresh = .01f) {

		if (!_calculateSpectrum) return -1f;

		float max = 0f;
		int index = 0;

		for (int i = 0; i < _spectrumData.Length; i++) {
			if (_spectrumData[i] > max) {
				max = _spectrumData[i];
				index = i;
			}
		}

		if (max > thresh)
			return index * _frequencyResolution;
		else
			return -1f;

	}

	void DrawDebugLines() {

		float[] spectrum = GetSpectrum(_debugSpectrum);

        for (int i = 1; i < spectrum.Length-1; i++) {
            Debug.DrawLine(transform.TransformPoint(new Vector3((i - 1)/100f, spectrum[i], 0)), transform.TransformPoint(new Vector3(i/100f, spectrum[i + 1], 0)), Color.gray);
            Debug.DrawLine(transform.TransformPoint(new Vector3(Mathf.Log(i - 1), spectrum[i - 1], 1f)), transform.TransformPoint(new Vector3(Mathf.Log(i), spectrum[i], 1f)), Color.white);
        }

		foreach ( FreqRange fr in _freqRanges ) {
			int minIndex = (int) ( fr._minHz / _frequencyResolution );
			int maxIndex = (int) ( fr._maxHz / _frequencyResolution );

			if ( fr._type == SpectrumType.Spectrum || fr._type == SpectrumType.SmoothedSpectrum ) {
				for ( int i = minIndex; i < maxIndex; i++ ) {
					Debug.DrawLine( transform.TransformPoint( new Vector3( ( i - 1 ) / 100f, spectrum[i], 0 ) ), transform.TransformPoint( new Vector3( i / 100f, spectrum[i + 1], 0f ) ), fr._debugColor );
				}
			} else {
				for ( int i = minIndex + 1; i < maxIndex; i++ ) {
					Debug.DrawLine( transform.TransformPoint( new Vector3( Mathf.Log( i - 1 ), spectrum[i - 1], 1f ) ), transform.TransformPoint( new Vector3( Mathf.Log( i ), spectrum[i], 1f ) ), fr._debugColor );
				}
			}
		}

	}

	void OnDrawGizmos() {
		if ( _drawGizmos ) {
			Vector3 cubeOffset = transform.TransformPoint( new Vector3(0f,0f,0f) );
			Vector3 spacing = transform.right * 0.5f;

			foreach ( FreqRange fr in _freqRanges ) {
				Gizmos.color = fr._debugColor;

				cubeOffset.y = fr._average * 0.5f + transform.position.y;
				Gizmos.DrawCube( cubeOffset, new Vector3( .5f, fr._average, .5f ) );
				cubeOffset += spacing;
			}
		}
	}

	float[] GetSpectrum(SpectrumType type) {
		float[] spectrum;
		if (type == SpectrumType.Spectrum) {
			spectrum = _spectrumData;
		} else if (type == SpectrumType.LogSpectrum) {
			spectrum = _logSpectrumData;
		} else if (type == SpectrumType.SmoothedSpectrum) {
			spectrum = _smoothSpectrumData;
		} else {
			spectrum = _logSmoothSpectrumData;
		}
		return spectrum;
	}

	public bool IsPlaying() {
		return _audioSource.isPlaying;
	}

}

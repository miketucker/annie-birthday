﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ParticleSystem))]
public class FlowParticles : MonoBehaviour {

	ParticleSystem ps;
	public Flow3D flow;
	public float etherMultiplier = 1f;
	ParticleSystem.Particle[] particles;

	public float drag = 0.5f;
	// Use this for initialization
	void Start() {
		ps = GetComponent<ParticleSystem>();
		if (flow == null) flow = GetComponent<Flow3D>();


	}

	// Update is called once per frame
	void Update() {
		particles = new ParticleSystem.Particle[ps.particleCount];
		ps.GetParticles(particles);

		if (drag < 1f) {
			for (int i = 0; i < particles.Length; i++) {
				particles[i].velocity = particles[i].velocity * drag + flow.SampleLite(particles[i].position);
			}
		}
		else {
			for (int i = 0; i < particles.Length; i++) {
				particles[i].velocity = particles[i].velocity + flow.SampleLite(particles[i].position);
			}
		}
		ps.SetParticles(particles, particles.Length);
	}
}

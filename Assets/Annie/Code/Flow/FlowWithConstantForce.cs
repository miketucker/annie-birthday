﻿using UnityEngine;
using System.Collections;

public class FlowWithConstantForce : Flow3D {
	public Vector3 constantForce = Vector3.zero;


	public override Vector3 SampleLite( Vector3 position, float ampScale = 1f ) {

		float amp = amplitude * ampScale;

		if ( strength > 0f ) {
			position += offset;
			Vector3 point = new Vector3( position.z, position.y, position.x + morphOffset );
			NoiseSample sampleX = Noise.Value3D( point, frequency );
			point = new Vector3( position.x + 100f, position.z, position.y + morphOffset );
			NoiseSample sampleY = Noise.Value3D( point, frequency );
			point = new Vector3( position.y, position.x + 100f, position.z + morphOffset );
			NoiseSample sampleZ = Noise.Value3D( point, frequency );
			return new Vector3(
				amp * ( sampleZ.derivative.x - sampleY.derivative.y ),
				amp * ( sampleX.derivative.x - sampleZ.derivative.y + ( 1f / ( 1f + position.y ) ) ),
				amp * ( sampleY.derivative.x - sampleX.derivative.y ) ) + constantForce;
		} else {
			return constantForce;
		}
	}
}

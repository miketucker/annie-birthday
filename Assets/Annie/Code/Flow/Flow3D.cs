﻿using UnityEngine;

public class Flow3D : MonoBehaviour {
	public string description = "Flow";
	public float morphSpeed;

	[Range(0f, 4f)]
	public float strength = 1f;

	public bool damping;

	public float frequency = 1f;

	[Range(1, 8)]
	public int octaves = 1;

	[Range(1f, 4f)]
	public float lacunarity = 2f;

	[Range(0f, 1f)]
	public float persistence = 0.5f;

	public bool useOffset = false;
	public bool useDeltaTime = false;
	public Vector3 offset = new Vector3(0f,1f,0f);


	public NoiseMethodType type;

	protected ParticleSystem system;
	protected ParticleSystem.Particle[] particles;

	protected float morphOffset;
	protected NoiseMethod method;

	protected float amplitude;
	protected Quaternion q;
	protected Quaternion qInv;

	void OnEnable() {
		method = Noise.methods[(int) type][2];

	}

	private void LateUpdate() {
		amplitude = damping ? strength / frequency : strength;

		morphOffset += Time.deltaTime * morphSpeed;

		if( morphOffset > 256f ) {
			morphOffset -= 256f;
		}

	}

	public Vector3 Sample(Vector3 position) {
		if(useOffset) position += offset;
		if (strength > 0f) {
			Vector3 point =new Vector3(position.z, position.y, position.x + morphOffset);
			NoiseSample sampleX = Noise.Sum(method, point, frequency, octaves, lacunarity, persistence);
			point = new Vector3(position.x + 100f, position.z, position.y + morphOffset);
			NoiseSample sampleY = Noise.Sum(method, point, frequency, octaves, lacunarity, persistence);
			point = new Vector3(position.y, position.x + 100f, position.z + morphOffset);
			NoiseSample sampleZ = Noise.Sum(method, point, frequency, octaves, lacunarity, persistence);
			return new Vector3(
				amplitude * (sampleZ.derivative.x - sampleY.derivative.y),
				amplitude * (sampleX.derivative.x - sampleZ.derivative.y + (1f / (1f + position.y))),
				amplitude * (sampleY.derivative.x - sampleX.derivative.y));
		} else {
			return Vector3.zero;
		}
	}

	public float SampleFloat(Vector3 position ) {
		Vector3 point = new Vector3( position.z, position.y, position.x + morphOffset );
		NoiseSample sampleX = Noise.Value3D( point, frequency );
		return (sampleX.derivative.x + sampleX.derivative.y + sampleX.derivative.z ) * strength;
	}

	public virtual Vector3 SampleLite(Vector3 position, float ampScale = 1f) {

		float amp = amplitude * ampScale;
		if ( useDeltaTime )
			amp *= Time.deltaTime;

		if (strength > 0f) {
			position += offset;
			Vector3 point =new Vector3(position.z, position.y, position.x + morphOffset);
			NoiseSample sampleX = Noise.Value3D(point, frequency);
			point = new Vector3(position.x + 100f, position.z, position.y + morphOffset);
			NoiseSample sampleY = Noise.Value3D(point, frequency);
			point = new Vector3(position.y, position.x + 100f, position.z + morphOffset);
			NoiseSample sampleZ = Noise.Value3D(point, frequency);
			return new Vector3(
				amp * (sampleZ.derivative.x - sampleY.derivative.y),
				amp * (sampleX.derivative.x - sampleZ.derivative.y + (1f / (1f + position.y))),
				amp * (sampleY.derivative.x - sampleX.derivative.y));
		} else {
			return Vector3.zero;
		}
	}

	private void PositionParticles() {
		for( int i = 0; i < particles.Length; i++ ) {
			Vector3 position = particles[i].position;
			particles[i].velocity = Sample(particles[i].position);
		}
	}
}
﻿Shader "Custom/Rim Colored (Vert)" 
{
	Properties 
	{
		_Color ("Base Color", Color) = (.5,.5,.5,1)
		_RimColor ("Rim Color", Color) = (0.26,0.19,0.16,0.0)
		_RimPower ("Rim Power", Range(0.1,8.0)) = 3.0
		_RimStrength("Rim Strength", Range(0.0,4.0)) = 0.25
		_AmbientLight ("Ambient Light", Range(0.0,1.0)) = 0.25
		_AmbientColor("Ambient Color", Color) = (0.0,0.0,0.0,1.0)
	}
	SubShader 
	{
		Tags { "Queue" = "Transparent" }

		//
		// This pass only writes to the depth buffer, incorporating the vertex modification
		//
		Pass {
			ZWrite On
			ColorMask 0

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			float _ZFreq;
			float _NormalAmp;
			float _Offset;

			struct appdata {
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v) {
				v2f o;
				//v.vertex.xyz += v.normal * (sin(_Time.y + _Offset + v.vertex.z * _ZFreq) * _NormalAmp + (_NormalAmp * 0.5));
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				return o;
			}

			fixed4 frag (v2f i) : SV_Target {
				return fixed4 (1.0, 1.0, 1.0, 1.0);
			}

				ENDCG
	}

		CGPROGRAM
#pragma surface surf Lambert vertex:vert alpha:fade
#pragma exclude_renderers d3d11_9x
		
		float4 _Color;
		float4 _RimColor;
		float4 _AmbientColor;
		float _RimPower;
		float _AmbientLight;
		float _RimStrength;

		struct Input
		{
			
			float4 albedoColor;
			float3 rimColor;
			float alpha;
			float rimPower;
			float3 rimCombo;
		};


		void vert (inout appdata_full v, out Input o) {
			UNITY_INITIALIZE_OUTPUT(Input,o);



			float3 viewDir = normalize(ObjSpaceViewDir(v.vertex));
			float dotProduct = saturate(1 - dot(v.normal, viewDir));
			half p = pow(dotProduct, _RimPower);
			
			o.alpha = _Color.a + p;
			o.rimPower = p;
			o.rimColor = lerp(_AmbientColor,_Color,p);
			o.rimCombo = (_Color * _RimColor * (_AmbientLight + _RimStrength)).rgb;

			//o.albedoColor = _Color;// lerp(_InnerColor, _OuterColor, abs(sin(length(v.vertex) + abs(sin(_Time.y)))));
			//o.rimColor = lerp(_RimColor,_RimOuterColor,abs(sin(length(v.vertex) + sin(_Time.y) ) ));
			//o.rimColor = _RimColor;
			//v.customColor = float2(_GradOffset,_GradOffset);

			//v.vertex.xyz += v.normal * (sin(_Time.y + _Offset + v.vertex.z * _ZFreq) * _NormalAmp + ( _NormalAmp * 0.5));
		}


		void surf (Input IN, inout SurfaceOutput o)
		{
			o.Albedo = IN.rimColor;
			o.Alpha = IN.alpha;
			o.Emission = lerp(_AmbientColor,IN.rimCombo,IN.rimPower);

			//o.Emission = float4(IN.customColor.x,IN.customColor.x,1,1);//o.Albedo;
			//o.Emission = o.Albedo;
		}
		ENDCG
	}
	Fallback "Diffuse"
}
﻿using UnityEngine;
using System.Collections;

public class SineMove : MonoBehaviour {

	Vector3 startPos;
	public Vector3 shiftAmount;
	public Vector3 shiftSpeed;
	public float globalSpeed = 0.1f;
	public bool useCosine = false;
	public bool useCircular = false;

	float timeElapsed = 0f;

	// Use this for initialization
	void Start () {
		startPos = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {

		Vector3 amt = shiftAmount;

		timeElapsed += Time.deltaTime * globalSpeed;
		float t = timeElapsed;

		if ( useCircular ) {
			amt.x *= Mathf.Cos( t * shiftSpeed.x );
			amt.y *= Mathf.Cos( t * shiftSpeed.y );
			amt.z *= Mathf.Sin( t * shiftSpeed.z );
								
		} else if ( useCosine) {
			amt.x *= Mathf.Cos (t * shiftSpeed.x);
			amt.y *= Mathf.Cos (t * shiftSpeed.y);
			amt.z *= Mathf.Cos (t * shiftSpeed.z);
								
		} else {				
			amt.x *= Mathf.Sin (t * shiftSpeed.x);
			amt.y *= Mathf.Sin (t * shiftSpeed.y);
			amt.z *= Mathf.Sin (t * shiftSpeed.z);
		}
		transform.localPosition = startPos + amt;
	}

	Vector3 Calculate(float offset ) {
		Vector3 amt = shiftAmount;
		if ( useCircular ) {
			amt.x *= Mathf.Cos( offset * shiftSpeed.x );
			amt.y *= Mathf.Cos( offset * shiftSpeed.y );
			amt.z *= Mathf.Sin( offset * shiftSpeed.z );
		} else if ( useCosine ) {
			amt.x *= Mathf.Cos( offset * shiftSpeed.x );
			amt.y *= Mathf.Cos( offset * shiftSpeed.y );
			amt.z *= Mathf.Cos( offset * shiftSpeed.z );

		} else {
			amt.x *= Mathf.Sin( offset * shiftSpeed.x );
			amt.y *= Mathf.Sin( offset * shiftSpeed.y );
			amt.z *= Mathf.Sin( offset * shiftSpeed.z );
		}
		return amt;
	}

	void OnDrawGizmosSelected() {
		if ( !Application.isPlaying ) {
			startPos = transform.localPosition;
		}
		int previewCount = 200;
		if ( transform.parent != null ) {
			for ( int i = 0; i < previewCount; i++ ) {
				Vector3 pos = Calculate( i * 0.15f );
				Gizmos.DrawCube( transform.parent.TransformPoint( startPos + ( pos ) ), Vector3.one * 0.01f );
			}
		} else {
			for ( int i = 0; i < previewCount; i++ ) {
				Vector3 pos = Calculate( i * 0.15f );
				Gizmos.DrawCube(startPos + ( pos ), Vector3.one * 0.01f );
			}
		}


	}
}

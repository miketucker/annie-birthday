#include "Common.cginc"

half _Metallic;
half _Smoothness;

half3 _BaseColor;
half _BaseRandom;

half _GlowIntensity;
half _GlowProb;
half3 _GlowColor;
half _GlowRandom;
half _HueMultiplier;
half _HueOffset;

half _SatMultiplier;
half _SatOffset;

half _ValMultiplier;
half _ValOffset;



struct Input
{
    half filamentID;
};

float3 hsv_to_rgb(float3 HSV)
{
	float3 RGB = HSV.z;

	float var_h = HSV.x * 6;
	float var_i = floor(var_h);   // Or ... var_i = floor( var_h )
	float var_1 = HSV.z * (1.0 - HSV.y);
	float var_2 = HSV.z * (1.0 - HSV.y * (var_h - var_i));
	float var_3 = HSV.z * (1.0 - HSV.y * (1 - (var_h - var_i)));
	if (var_i == 0) { RGB = float3(HSV.z, var_3, var_1); }
	else if (var_i == 1) { RGB = float3(var_2, HSV.z, var_1); }
	else if (var_i == 2) { RGB = float3(var_1, HSV.z, var_3); }
	else if (var_i == 3) { RGB = float3(var_1, var_2, HSV.z); }
	else if (var_i == 4) { RGB = float3(var_3, var_1, HSV.z); }
	else { RGB = float3(HSV.z, var_1, var_2); }

	return (RGB);
}


void vert(inout appdata_full v, out Input data)
{
    UNITY_INITIALIZE_OUTPUT(Input, data);

    float2 uv = v.texcoord.xy;

    // Orthonormal basis vectors
    float3x3 basis = DecodeBasis(SampleBasis(uv));

    // Apply the local transformation and move to the sampled position.
    v.vertex.xyz = SamplePosition(uv) + mul(v.vertex, basis) * Thickness(uv);
    v.normal = mul(v.normal, basis);

    // Parameters for the pixel shader
    data.filamentID = uv.x + _RandomSeed * 58.92128;
}

void surf(Input IN, inout SurfaceOutputStandard o)
{
    // Random color
	half3 hsv = half3(0, 0, 0);
	hsv.r = _HueOffset + frac(IN.filamentID * 412.123)* _HueMultiplier;
	hsv.g = _SatOffset + frac(IN.filamentID * 412.123) * _SatMultiplier;
	hsv.b = _ValOffset + frac(IN.filamentID * 412.123) * _ValMultiplier;
    //half3 color = HueToRGB();
	half3 color = hsv_to_rgb(hsv);

    // Glow effect
    half glow = frac(IN.filamentID * 138.9044 + _Time.y / 2) < _GlowProb;

    o.Albedo = lerp(_BaseColor, color, _BaseRandom);
    o.Smoothness = _Smoothness;
    o.Metallic = _Metallic;
    o.Emission = lerp(_GlowColor, color, _GlowRandom) * _GlowIntensity * glow;
}
